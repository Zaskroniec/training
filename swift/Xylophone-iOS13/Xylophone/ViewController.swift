//
//  ViewController.swift
//  Xylophone
//
//  Created by Angela Yu on 28/06/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    var player: AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func keyPressed(_ sender: UIButton) {
        guard let buttonTile: String = sender.currentTitle else { return }
        
        playSound(soundName: buttonTile)
        changeOpacity(button: sender)
    }
    
    
    func playSound(soundName: String) {
        let resourceUrl = Bundle.main.url(forResource: soundName, withExtension: "wav")
        player = try! AVAudioPlayer(contentsOf: resourceUrl!)
        player.play()
    }
    
    func changeOpacity(button: UIButton) {
        button.alpha = 0.5
    }
}

