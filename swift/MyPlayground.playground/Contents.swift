// Try reduce function

let list: Array<Int32> = [1, 2, 3, 4, 5]
let sum: Int32? = list.reduce(into: 0) { (memo, number) in
    memo += number
}

print(sum!)

// Tuples
let tuple_one = (404, "test")
let (code_one, message_one) = tuple_one
print(code_one, message_one)

let tuple_two = (code: 404, message: "test")
print(tuple_two.code, tuple_two.message)

let tuple_three = (500, "internal message error")
let(code_three, _) = tuple_three
print(code_three)

// Optionals
var some_number: Int?
some_number = 32 // here is an example to unwrap optional variable, because we know that variable is not nil
print(some_number!)

var some_string: String?
if some_string == nil {
    print("Is not a string")
} else {
    print("Is a string")
}

func triggerComment() {
    print("Comment")
}

(1...3).forEach { (_) in
    triggerComment()
}

func addNumbers(number_one: Int, number_two: Int) -> Int {
    return number_one + number_two
}

print(addNumbers(number_one: 7, number_two: 8))

func multipleNumbers(_ var_a: Int, _ var_b: Int) -> Int {
    let result = var_a * var_b
    
    return result
}

print(multipleNumbers(3, 3))
