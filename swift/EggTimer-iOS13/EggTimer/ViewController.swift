//
//  ViewController.swift
//  EggTimer
//
//  Created by Angela Yu on 08/07/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    @IBOutlet weak var eggLabel: UILabel!
    @IBOutlet weak var eggProgress: UIProgressView!
    
    let defaultTitle: String = "How do you like your eggs?"
    let boilingTimeOptions: [String:Int] = ["soft": 300, "medium": 420, "hard": 720]
    var timer = Timer()
    var player: AVAudioPlayer!
    var timeRemainig = 300
    var startTime = 300
    
    @IBAction func hardnessSelected(_ sender: UIButton) {
        setStartState()
        
        guard let hardness: String = sender.currentTitle else { return }
        let hardnessType = hardness.lowercased()
        
        eggLabel.text = hardness
        timeRemainig = boilingTimeOptions[hardnessType]!
        startTime = boilingTimeOptions[hardnessType]!
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (_) in
            self.count()
        }
    }
    
    func count() {
        timeRemainig -= 1
        fillProgress()
        
        if timeRemainig == 0 {
            eggLabel.text = "Done"
            player.play()
            stopAppTimers()
        }
    }
    
    func stopAppTimers() {
        timer.invalidate()
    }
    
    func fillProgress() {
        let result = (Double(timeRemainig) / Double(startTime))
        
        eggProgress.progress = Float(result)
    }
    
    func setStartState() {
        guard let url = Bundle.main.url(forResource: "alarm_sound", withExtension: "mp3") else { return }
        
        stopAppTimers()
        eggLabel.text = defaultTitle
        eggProgress.progress = 1.0
        player = try! AVAudioPlayer(contentsOf: url)
    }
}
