//
//  ViewController.swift
//  Dicee-iOS13
//
//  Created by Angela Yu on 11/06/2019.
//  Copyright © 2019 London App Brewery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // Refer IBOutlet to define UI components
    @IBOutlet weak var diceImageViewRight: UIImageView!
    @IBOutlet weak var diceImageViewLeft: UIImageView!
    
    let diceCollection: Array = [#imageLiteral(resourceName: "DiceOne"), #imageLiteral(resourceName: "DiceTwo"), #imageLiteral(resourceName: "DiceThree"), #imageLiteral(resourceName: "DiceFour"), #imageLiteral(resourceName: "DiceFive"), #imageLiteral(resourceName: "DiceSix")]
    let startingDice = 0
    let defaultAplha = CGFloat(0.5)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Define boot images
        diceImageViewLeft.image  = diceCollection[startingDice]
        diceImageViewRight.image = diceCollection[startingDice]
        
        // Set components attributes (similar to sidebar attributes)
        diceImageViewLeft.alpha = defaultAplha
        diceImageViewRight.alpha = defaultAplha
    }

    @IBAction func rollButtonPressed(_ sender: UIButton) {
        diceImageViewLeft.image = diceCollection.randomElement()
        diceImageViewRight.image = diceCollection.randomElement()
    }
}

