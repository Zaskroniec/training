-- Create the directors table
CREATE TABLE IF NOT EXISTS directors (
  id SERIAL PRIMARY KEY,
  first_name VARCHAR(30),
  last_name VARCHAR(30) NOT NULL,
  date_of_birth DATE,
  nationality VARCHAR(20)
);

-- Create the actors table
CREATE TABLE IF NOT EXISTS actors (
  id SERIAL PRIMARY KEY,
  first_name VARCHAR(30),
  last_name VARCHAR(30) NOT NULL,
  gender CHAR(1),
  date_of_birth DATE
);

-- Create the movies table
CREATE TABLE IF NOT EXISTS movies (
  id SERIAL PRIMARY KEY,
  director_id INT REFERENCES directors (id),
  name VARCHAR(50) NOT NULL,
  length INT,
  language VARCHAR(20),
  release_date DATE,
  age_certificate VARCHAR(5)
);

-- Create the movie_revenue table
CREATE TABLE IF NOT EXISTS movie_revenues (
  id SERIAL PRIMARY KEY,
  movie_id INT REFERENCES movies (id),
  domestic_takings NUMERIC(6, 2),
  international_takings NUMERIC(6, 2)
);

-- Create the movie_actors table
CREATE TABLE IF NOT EXISTS movie_actors (
  movie_id INT REFERENCES movies (id),
  actor_id INT REFERENCES actors (id),
  PRIMARY KEY (movie_id, actor_id)
);
