-- Create user
CREATE USER movie_director
  WITH SUPERUSER
    LOGIN
    CREATEDB
    PASSWORD 'password';

-- Create Database
CREATE DATABASE movie_data
  WITH OWNER movie_director
    TEMPLATE template0;
