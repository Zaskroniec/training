defmodule Fizzbuzz do
  defstruct fizz: 0, buzz: 0, fizzbuzz: 0, nothing: 0

  def play(variant) do
    apply(Fizzbuzz, variant, [])
  end

  def play_variant_one do
    number_list()
    |> Enum.reduce(%Fizzbuzz{}, fn number, acc -> div_range(number, acc) end)
  end

  def play_variant_two do
    number_list()
    |> Enum.reduce(%Fizzbuzz{}, fn number, acc ->
      matcher({dividable?(number, 3), dividable?(number, 5), acc})
    end)
  end

  def matcher({true, true, acc}) do
    IO.puts("fizzbuzz")
    update_fizzbuzz(acc, :fizzbuzz)
  end

  def matcher({true, _, acc}) do
    IO.puts("fizz")
    update_fizzbuzz(acc, :fizz)
  end

  def matcher({_, true, acc}) do
    IO.puts("buzz")
    update_fizzbuzz(acc, :buzz)
  end

  def matcher({_, _, acc}) do
    update_fizzbuzz(acc, :nothing)
  end

  defp div_range(number, %Fizzbuzz{} = acc) do
    cond do
      dividable?(number, 3) && dividable?(number, 5) ->
        IO.puts("fizzbuzz")
        update_fizzbuzz(acc, :fizzbuzz)

      dividable?(number, 3) ->
        IO.puts("fizz")
        update_fizzbuzz(acc, :fizz)

      dividable?(number, 5) ->
        IO.puts("buzz")
        update_fizzbuzz(acc, :buzz)

      true ->
        update_fizzbuzz(acc, :nothing)
    end
  end

  defp number_list do
    1..20
    |> Enum.to_list()
  end

  defp update_fizzbuzz(%Fizzbuzz{} = acc, key), do: Map.update!(acc, key, &(&1 + 1))

  defp dividable?(number, target), do: Integer.mod(number, target) === 0
end
