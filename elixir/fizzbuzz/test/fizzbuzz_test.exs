defmodule FizzbuzzTest do
  use ExUnit.Case

  alias Fizzbuzz

  describe "Fizzbuzz.play_variant_one" do
    setup do
      %{subject: Fizzbuzz.play_variant_one()}
    end

    test "returns counted fizz", context do
      assert context[:subject].fizz == 5
    end

    test "returns counted buzz", context do
      assert context[:subject].buzz == 3
    end

    test "returns counted fizzbuzz", context do
      assert context[:subject].fizzbuzz == 1
    end

    test "returns counted miss", context do
      assert context[:subject].nothing == 11
    end
  end

  describe "Fizzbuzz.play_variant_two" do
    setup do
      %{subject: Fizzbuzz.play_variant_two()}
    end

    test "returns counted fizz", context do
      assert context[:subject].fizz == 5
    end

    test "returns counted buzz", context do
      assert context[:subject].buzz == 3
    end

    test "returns counted fizzbuzz", context do
      assert context[:subject].fizzbuzz == 1
    end

    test "returns counted miss", context do
      assert context[:subject].nothing == 11
    end
  end
end
