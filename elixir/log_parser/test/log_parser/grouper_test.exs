defmodule LogParserTest.GrouperTest do
  use ExUnit.Case

  alias LogParser.Grouper

  setup do
    list = [
      %{endpoint: "/api/v1/users", ip: "192.168.11.1"},
      %{endpoint: "/api/v1/users", ip: "192.168.11.2"},
      %{endpoint: "/api/v1/users", ip: "192.168.11.1"},
      %{endpoint: "/api/v1/posts", ip: "192.168.11.1"}
    ]

    %{subject: Grouper.generate_groups(list)}
  end

  describe "LogParser.Grouper.group_visits/1" do
    test "returns total visits per endpoint", context do
      expected_result = [
        %LogParser.Group{name: "/api/v1/posts", visits: 1},
        %LogParser.Group{name: "/api/v1/users", visits: 3}
      ]

      assert context[:subject][:total] == expected_result
    end

    test "returns total uniq visits per endpoint", context do
      expected_result = [
        %LogParser.Group{name: "/api/v1/posts", visits: 1},
        %LogParser.Group{name: "/api/v1/users", visits: 2}
      ]

      assert context[:subject][:uniq] == expected_result
    end

    test "returns average visits per endpoint", context do
      expected_result = [
        %LogParser.Group{name: "/api/v1/posts", visits: 0.25},
        %LogParser.Group{name: "/api/v1/users", visits: 0.5}
      ]

      assert context[:subject][:average] == expected_result
    end
  end
end
