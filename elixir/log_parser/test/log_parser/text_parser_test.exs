defmodule LogParserTest.TextParserTest do
  use ExUnit.Case

  alias LogParser.TextParser

  setup do
    log_data = """
    /api/v1/users 192.168.11.1
    /api/v1/users 192.168.11.3
    """

    %{subject: TextParser.parse(log_data)}
  end

  describe "TextParser/parse1" do
    test "returns valid structure", context do
      expected_result = [
        %{endpoint: "/api/v1/users", ip: "192.168.11.1"},
        %{endpoint: "/api/v1/users", ip: "192.168.11.3"}
      ]

      assert context[:subject] == expected_result
    end
  end
end
