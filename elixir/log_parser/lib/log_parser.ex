defmodule LogParser do
  alias LogParser.TextParser
  alias LogParser.Grouper

  def call(text) do
    TextParser.parse(text)
    |> Grouper.generate_groups()
  end
end
