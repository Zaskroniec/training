defmodule LogParser.TextParser do
  def parse(text) do
    text
    |> split_text("\n")
    |> map_rows
  end

  defp split_text(text, method) do
    String.split(text, method, trim: true)
  end

  defp map_rows(rows_list) do
    Enum.map(rows_list, fn row ->
      split_text(row, " ")
      |> build_structure
    end)
  end

  defp build_structure([endpoint, ip]) do
    %{endpoint: endpoint, ip: ip}
  end
end
