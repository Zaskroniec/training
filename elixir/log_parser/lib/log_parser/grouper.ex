defmodule LogParser.Grouper do
  alias LogParser.Group
  import Decimal, only: [div: 2, to_float: 1]

  def generate_groups(list) do
    Enum.group_by(list, fn item -> item[:endpoint] end)
    |> build_counted_groups
  end

  defp build_counted_groups(groups) do
    total = count_total_visits(groups)
    uniq = count_uniq_visits(groups)
    average = count_average_visits(uniq, total)

    %{
      total: total,
      uniq: uniq,
      average: average
    }
  end

  defp count_total_visits(groups) do
    Enum.map(groups, fn {group_name, group_collection} ->
      %Group{
        name: group_name,
        visits: length(group_collection)
      }
    end)
  end

  defp count_uniq_visits(groups) do
    Enum.map(groups, fn {group_name, group_collection} ->
      %Group{
        name: group_name,
        visits: length(Enum.uniq(group_collection))
      }
    end)
  end

  defp count_average_visits(uniq_visits, total_visits) do
    Enum.map(uniq_visits, fn %Group{name: name, visits: visits} ->
      %Group{
        name: name,
        visits: Decimal.div(visits, sum_all_visits(total_visits)) |> Decimal.to_float()
      }
    end)
  end

  defp sum_all_visits(total_visits) do
    Enum.reduce(total_visits, 0, fn %Group{name: _name, visits: visits}, acc ->
      visits + acc
    end)
  end
end
