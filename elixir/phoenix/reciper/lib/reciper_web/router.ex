defmodule ReciperWeb.Router do
  use ReciperWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ReciperWeb do
    pipe_through :browser

    # get "/", PageController, :index
    # get "/recipes", RecipeController, :index
    # get "/recipes/:id", RecipeController, :show
    resources "/recipes", RecipeController, only: [:index, :show]

    get "/", RecipeController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", ReciperWeb do
  #   pipe_through :api
  # end
end
