defmodule Reciper.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      Reciper.Repo,
      # Start the endpoint when the application starts
      ReciperWeb.Endpoint
      # Starts a worker by calling: Reciper.Worker.start_link(arg)
      # {Reciper.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Reciper.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    ReciperWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
