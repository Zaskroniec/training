# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :reciper,
  ecto_repos: [Reciper.Repo]

# Configures the endpoint
config :reciper, ReciperWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "qVgyHBWPWr1vbjaf2KXVSXzMblqC4DUb9m3e1sVDwoAmXrh7FFIzQS4aIcB+lN7o",
  render_errors: [view: ReciperWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Reciper.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
