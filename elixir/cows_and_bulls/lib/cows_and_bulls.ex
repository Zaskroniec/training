defmodule CowsAndBulls do
  defstruct cows: 0, bulls: 0, fails: 0

  def start_game do
    generate_target_result() |> play
  end

  defp play(target_result) do
    input = parse_input()

    if length(input) < 4, do: play(target_result)

    result =
      Enum.reduce(Enum.with_index(target_result), %CowsAndBulls{}, fn {target_number, index},
                                                                      acc ->
        input_position_number = Enum.fetch(input, index)

        cond do
          match_target_number?(target_number, input_position_number) ->
            update_shots(:cows, acc)

          is_member?(target_result, input_position_number) ->
            update_shots(:bulls, acc)

          !is_member?(target_result, input_position_number) ->
            update_shots(:fails, acc)
        end
      end)

    summary(result, target_result)
  end

  defp parse_input do
    IO.gets(:stdio, "Type your guess (4 digits): ")
    |> String.trim()
    |> String.codepoints()
    |> Enum.map(fn x -> String.to_integer(x) end)
  end

  defp generate_target_result do
    0..9
    |> Enum.to_list()
    |> Enum.shuffle()
    |> Enum.slice(0, 4)
  end

  defp match_target_number?(target_number, {_, input_number}) do
    target_number === input_number
  end

  defp is_member?(list, {:ok, number}) do
    Enum.member?(list, number)
  end

  defp update_shots(key, %CowsAndBulls{} = acc) do
    (fn
       :cows -> Map.update!(acc, key, &(&1 + 1))
       :bulls -> Map.update!(acc, key, &(&1 + 1))
       _ -> Map.update!(acc, key, &(&1 + 1))
     end).(key)
  end

  defp summary(%CowsAndBulls{} = result, target_result) do
    (fn
       %{cows: 4} ->
         :done

       _ ->
         IO.inspect(result)
         play(target_result)
     end).(result)
  end
end
