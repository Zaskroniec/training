# CowsAndBulls

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `cows_and_bulls` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:cows_and_bulls, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/cows_and_bulls](https://hexdocs.pm/cows_and_bulls).

