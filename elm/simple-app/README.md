# Planning Basketball Scorekeeper App

## Model

TODO: Model's Shape

```
Model =
  { players : List Player
  , playerNameInput : String
  , plays : List Play  
  , selectedPlayerId : Maybe Int
  }
```

TODO: Player Shape

```
Player =
  { id : Int
  , name : String
  , points : Int
  }
```

TODO: Play Shape

```
Play =
  { id : Int
  , playerId : Int
  , playerName : String
  , points : Int
  }
```

TODO: Initial Model values

## Update

* Edit
* Score
* Input
* Save
* Cancel
* Delete Play

TODO: Create Message Union Type

TODO: Create update function(s)

## View

* main view ( outermost function)
  * player table
    * players head
      * name
      * blank
      * points      
    * players body
      * name (with edit action)
      * score actions
      * total scores
  * player form
  * plays table
    * plays head
      * title
      * points
    * plays body
      * name (with delete action)
      * score

TODO: Create function for each of the above
