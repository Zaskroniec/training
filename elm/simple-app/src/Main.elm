module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


-- model


type alias Model =
    { players : List Player
    , plays : List Play
    , playerNameInput : String
    , selectedPlayerId : Maybe Int
    }


type alias Player =
    { id : Int
    , name : String
    , points : Int
    }


type alias Play =
    { id : Int
    , playerId : Int
    , playerName : String
    , points : Int
    }



-- init model


initModel : Model
initModel =
    { players = []
    , playerNameInput = ""
    , selectedPlayerId = Nothing
    , plays = []
    }



-- update


type Msg
    = Edit Player
    | Score Player Int
    | Input String
    | Save
    | Cancel
    | DeletePlay Play



-- update


update : Msg -> Model -> Model
update msg model =
    case msg of
        Edit player ->
            { model
                | playerNameInput = player.name
                , selectedPlayerId = Just player.id
            }

        Score player points ->
            score model player points

        Input playerName ->
            { model | playerNameInput = playerName }

        Save ->
            if String.isEmpty model.playerNameInput then
                model
            else
                save model

        Cancel ->
            resetInput model

        DeletePlay play ->
            deletePlay model play


resetInput : Model -> Model
resetInput model =
    { model | playerNameInput = "", selectedPlayerId = Nothing }


score : Model -> Player -> Int -> Model
score model scorer newPoints =
    let
        newPlayers =
            List.map
                (\player ->
                    if scorer.id == player.id then
                        { player | points = player.points + newPoints }
                    else
                        player
                )
                model.players

        play =
            Play (List.length model.plays) scorer.id scorer.name newPoints

        newPlays =
            play :: model.plays
    in
    { model
        | players = newPlayers
        , plays = newPlays
    }


deletePlay : Model -> Play -> Model
deletePlay model play =
    let
        newPlays =
            List.filter (\p -> p.id /= play.id) model.plays

        newPlayers =
            List.map
                (\player ->
                    if player.id == play.playerId then
                        { player | points = player.points - 1 * play.points }
                    else
                        player
                )
                model.players
    in
    { model
        | players = newPlayers
        , plays = newPlays
    }


save : Model -> Model
save model =
    case model.selectedPlayerId of
        Just id ->
            editMode model id

        Nothing ->
            addMode model


editMode : Model -> Int -> Model
editMode model id =
    let
        newPlayers =
            List.map
                (\player ->
                    if player.id == id then
                        { player | name = model.playerNameInput }
                    else
                        player
                )
                model.players

        newPlays =
            List.map
                (\play ->
                    if play.playerId == id then
                        { play | playerName = model.playerNameInput }
                    else
                        play
                )
                model.plays
    in
    { model
        | players = newPlayers
        , plays = newPlays
    }


addMode : Model -> Model
addMode model =
    let
        player =
            Player (List.length model.players) model.playerNameInput 0

        newPlayers =
            player :: model.players
    in
    { model
        | players = newPlayers
        , playerNameInput = ""
    }


totalPoints : Model -> Int
totalPoints model =
    model.players
        |> List.map .points
        |> List.sum



-- view


view : Model -> Html Msg
view model =
    div [ class "container" ]
        [ h1 [] [ text "Scope Board" ]
        , playersTable model
        , playerForm model
        , playsTable model
        ]


playersTable : Model -> Html Msg
playersTable model =
    div [ class "players-wrapper" ]
        [ playersHead
        , playersBody model
        , playersTotalPoints model
        ]


playersHead : Html Msg
playersHead =
    thead []
        [ tr []
            [ th [] [ text "Name" ]
            , th [] []
            , th [] [ text "Points" ]
            ]
        ]


playersBody : Model -> Html Msg
playersBody model =
    model.players
        |> List.sortBy .name
        |> List.map player
        |> tbody []


player : Player -> Html Msg
player player =
    tr []
        [ td [ onClick (Edit player) ] [ text player.name ]
        , td []
            [ button
                [ type_ "button"
                , onClick (Score player 2)
                ]
                [ text "Score 2" ]
            , button
                [ type_ "button"
                , onClick (Score player 5)
                ]
                [ text "Score 5" ]
            ]
        , td [] [ text (toString player.points) ]
        ]


playersTotalPoints : Model -> Html Msg
playersTotalPoints model =
    tbody []
        [ tr []
            [ td [] []
            , td [] []
            , td [] [ text ("Total points: " ++ toString (totalPoints model)) ]
            ]
        ]


playsTable : Model -> Html Msg
playsTable model =
    div [ class "plays-wrapper" ]
        [ playsHead
        , playsBody model
        ]


playsHead : Html Msg
playsHead =
    thead []
        [ tr []
            [ th [] [ text "Name" ]
            , th [] [ text "Points" ]
            ]
        ]


playsBody : Model -> Html Msg
playsBody model =
    model.plays
        |> List.map playRow
        |> tbody []


playRow : Play -> Html Msg
playRow play =
    tr [ onClick (DeletePlay play) ]
        [ td [] [ text play.playerName ]
        , td [] [ text (toString play.points) ]
        ]


playerForm : Model -> Html Msg
playerForm model =
    Html.form [ onSubmit Save ]
        [ input
            [ type_ "text"
            , placeholder "Add/Edit player..."
            , onInput Input
            , value model.playerNameInput
            ]
            []
        , button [ type_ "submit" ] [ text "Save " ]
        , button [ type_ "button", onClick Cancel ] [ text "Cancel " ]
        ]



-- main Program


main : Program Never Model Msg
main =
    beginnerProgram
        { model = initModel
        , update = update
        , view = view
        }
