module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


-- model


type alias Model =
    { calories : Int
    , input : Int
    , errors : Maybe String
    }


initModel : Model
initModel =
    { calories = 0
    , input = 0
    , errors = Nothing
    }



-- update


type Msg
    = AddCalories
    | RemoveCalories
    | Input String
    | Clear


update : Msg -> Model -> Model
update msg model =
    case msg of
        AddCalories ->
            { model
                | calories = model.calories + model.input
                , input = 0
            }

        RemoveCalories ->
            let
                difference =
                    model.calories - model.input
            in
            if difference <= 0 then
                { model
                    | calories = 0
                    , input = 0
                }
            else
                { model
                    | calories = model.calories - model.input
                    , input = 0
                }

        Input value ->
            case String.toInt value of
                Ok input ->
                    { model
                        | input = input
                        , errors = Nothing
                    }

                Err error ->
                    { model
                        | input = 0
                        , errors = Just error
                    }

        Clear ->
            initModel


isDisabled : Int -> Bool
isDisabled calories =
    (<=) calories 0


parseValue : Int -> String
parseValue input =
    if input == 0 then
        ""
    else
        toString input



-- view


view : Model -> Html Msg
view model =
    div []
        [ h3 []
            [ text ("Total calories: " ++ toString model.calories) ]
        , input
            [ type_ "text"
            , onInput Input
            , value (parseValue model.input)
            ]
            []
        , div [] [ text (Maybe.withDefault "" model.errors) ]
        , button
            [ type_ "button"
            , onClick AddCalories
            ]
            [ text "Add calories" ]
        , button
            [ type_ "button"
            , onClick RemoveCalories
            , disabled (isDisabled model.calories)
            ]
            [ text "Remove Calories" ]
        , button
            [ type_ "button"
            , onClick Clear
            ]
            [ text "Clear" ]
        ]


main : Program Never Model Msg
main =
    beginnerProgram
        { model = initModel
        , update = update
        , view = view
        }
