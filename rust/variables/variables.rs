fn main() {

    // constant variable
    const FIRST_COST: u32 = 100_000;
    println!("{}", FIRST_COST);

    // mutable variable
    let mut x = 5;
    println!("{}", x);

    x = 6;
    println!("{}", x);

    //  immutable variable
    let y = 5;
    println!("{}", y);

    // shadowing
    let t = 5;
    let t = t + 1;
    let t = t * 2;
    println!("{}", t);

    // change variable type - if variable is defined as mutable then
    // we will get compile-time error i.e:
    // let mut spaces = " ";
    // spaces = spaces.len();
    let spaces = " ";
    let spaces = spaces.len();
    println!("{}", spaces);
}
