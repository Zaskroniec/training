#![allow(dead_code)]

use constants::errors;

pub fn parse_to_number(number: &str) -> Result<i64, &'static str> {
    match number.trim().parse() {
        Ok(number) => Ok(number),
        Err(_error) => {
            Err(errors::PARSER_ERROR)
        }
    }
}
