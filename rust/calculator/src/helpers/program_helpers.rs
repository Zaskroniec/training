#![allow(dead_code)]

use std::process;
use std::io;
use helpers::parsers;

pub fn exit_program(message: &str) {
    println!("{}", message);
    process::exit(1);
}

pub fn parse_input_to_int() -> Result<i64, &'static str> {
    let mut input = String::new();

    io::stdin().read_line(&mut input)
        .expect("input error!");

    match parsers::parse_to_number(&input.to_string()) {
        Ok(number) => Ok(number),
        Err(message) => Err(message)
    }
}
