// On start load selectable program options like
// 1. Start calculating
// create simple arithmetic calculations
// 2. Exit program
// run program options on request - i.e special character

mod calculator;
mod constants;
mod helpers;

fn main() {
    println!("Select options:\n1. Run calculator\n2. Exit program");
    let selected_option = helpers::program_helpers::parse_input_to_int();

    match selected_option {
        Ok(number) => {
            if number == 1 {
                calculator::run_program();
            } else {
                helpers
                    ::program_helpers
                    ::exit_program(constants::notices::EXIT_PROGRAM);
            }
        },
        Err(message) => helpers
                            ::program_helpers
                            ::exit_program(message)
    };
}
