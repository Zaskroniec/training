const CONSTANT_EXAMPLE: i64 = 33; // no fixed address, cannot be muttable

static mut STATIC_EXAMPLE: i64 = 22; // to muttable this static var you should use `unsafe` block

fn main() {
    println!("constant => {}", CONSTANT_EXAMPLE);

    unsafe {
        STATIC_EXAMPLE = 1;
        println!("static => {}", STATIC_EXAMPLE);
    }
}
