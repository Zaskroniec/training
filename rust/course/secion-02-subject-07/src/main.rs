fn main() {
    operators();
}

fn operators() {
    // arymethic operators
    let mut variable_first: i32 = 2 + 3 * 4;
    println!("{:?}", variable_first);
    variable_first += 1;
    println!("{}", variable_first);

    // remainders
    variable_first %= 2; // it will returns reminder after division -> %
    println!("{}", variable_first);

    // cubed
    let mut variable_second: i32 = 2;
    variable_second = i32::pow(variable_second, 3);
    println!("{}", variable_second);

    let variable_third: f64 = 2.5;
    let variable_third_powi = f64::powi(variable_third, 3);
    let variable_third_powf = f64::powf(variable_third, std::f64::consts::PI);
    println!("powi: {}", variable_third_powi);
    println!("powf: {}", variable_third_powf);

    // bitwise
    let variable_fourth = 1 | 2;
    println!("1 | 2 = {}", variable_fourth);
    let variable_fourth_shift = 1 << 10; // >>
    println!("2^10 = {}", variable_fourth_shift);
}
