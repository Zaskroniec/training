use std::mem;

const BYTES: usize = 8;

fn main() {

    // simple int assign and checking memory taken
    let number: i64 = 64;
    println!("number is: {} and it's size is: {} bytes", number, mem::size_of_val(&number));

    // isize/usize  it's size of the word content or i.e address in memory
    let next_number: isize = 100;
    let size_of_next_number = mem::size_of_val(&next_number);
    let os_size = size_of_next_number * BYTES;
    println!("next number is {} and it's size is {} bytes, addionaly it's {} of OS-bytes",
        next_number, size_of_next_number, os_size);

    let single_letter: char = 'x';
    let size_of_single_letter = mem::size_of_val(&single_letter);
    println!("single char: {} and it's size: {} bytes",
        single_letter, size_of_single_letter);

    let float_number: f64 = 2.55;
    let size_of_float_number = mem::size_of_val(&float_number);
    println!("float_number: {} and it's size: {} bytes",
        float_number, size_of_float_number);

    let bool_value: bool = false;
    let size_of_bool_value = mem::size_of_val(&bool_value);
    println!("bool number: {} and it's size: {} bytes",
        bool_value, size_of_bool_value);
}
