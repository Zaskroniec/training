fn main() {
    scope_and_shadowing();
}

fn scope_and_shadowing() {
    let a: i64 = 123;

    // let a: i64 = 123; it will override upper variable

    {
        let b: i64 = 100;
        println!("var inside a scope {}", b);
        let a: i64 = 5;
        println!("outside var inside a scope {}", a);
    }

    println!("{}", a);
}
