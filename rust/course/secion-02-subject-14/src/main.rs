fn main() {
    let country_code: i64 = 48;
    let country = match country_code {
        48 => "PL",
        44 => "UK",
        1...999 => "Matched",
        _ => "unknow"
    };

    println!("number: {}, iso: {}", country_code, country);
}
