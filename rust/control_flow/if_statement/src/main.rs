use std::io;

const HIGHER_NUMBER_MESSAGE: &str = "number is less than 100";
const LESSER_NUMBER_MESSAGE: &str = "number is higher than 100";

fn main() {
    loop {
        println!("Type your number...\n");
        let mut input = String::new();

        io::stdin().read_line(&mut input)
            .expect("Failed to read line");

        let input: i32 = match input.trim().parse() {
            Ok(num) => num,
            Err(error) => {
                display_on_screen(&error.to_string());
                continue;
            }
        };

        check_number(input);
    }
}

fn check_number(number: i32) {
    if number < 100 {
        display_on_screen(HIGHER_NUMBER_MESSAGE);
    } else {
        display_on_screen(LESSER_NUMBER_MESSAGE);
    }
}

fn display_on_screen(message: &str) { println!("message: {}", message); }
