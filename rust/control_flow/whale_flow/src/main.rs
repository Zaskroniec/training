const DEFAULT_CYCLES: i32 = 4;
const END_OF_CYCLES:  i32 = 0;
const CYCLE_VAR:      i32 = 1;

fn main() {
    println!("Start cycling process");

    let mut cycles = DEFAULT_CYCLES;

    while cycles > END_OF_CYCLES {
        println!("This is cycle No {}", cycles);
        cycles -= CYCLE_VAR;
    }
}
