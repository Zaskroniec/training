fn main() {
    // string type
    let string = "string";
    println!("{}", string);

    // unsigned integer type -> cannot be negative
    let uinteger: u32 = 32;
    println!("{}", uinteger);

    // signed integer type -> could be also positive
    let iinteger: i32 = -32;
    println!("{}", iinteger);

    // float type 64bit
    let float64 = 2.0; // default 64 bit
    println!("{}", float64);

    // float type 32bit
    let float32: f32 = 66.0;
    println!("{}", float32);

    // boolean type
    let truthy: bool = true;
    println!("{}", truthy);

    // Tuples
    let tuple: (i32, f64, u8) = (500, 6.4, 1);
    let (x, y, z) = tuple;
    println!("x: {}, y: {}, z: {}", x, y, z);

    // assign tuple variable using . (dot)
    let five_hundred = tuple.0;
    let six_point_four = tuple.1;
    let one = tuple.2;
    println!("five_hundred: {}, six_point_four: {}, one: {}",
        five_hundred, six_point_four, one);

    // Arrays - collection in arrays are static
    let default_array = [1, 2, 3, 4, 5];
    println!("print first element on array: {}", default_array[0]);

    // Define vector

    let mut vector = vec![]; // let mut vector = Vec::new();
    vector.push(1);
    vector.push(2);

    println!("first element of vector: {}", vector[0]);
    println!("second element of vector: {}", vector[1]);

    // define type in vector
    let mut vector_with_type: Vec<u32> = Vec::new();
    vector_with_type.push(33);
    println!("first element of vector_with_type: {}", vector_with_type[0]);
}
