# Read me

## Setup

*make sure that you have installed rust language and cargo package manager*

* run `cargo build` inside project
* run `./target/debug/hello_cargo`

You should get `Hello, cargo!` displayed in your terminal
