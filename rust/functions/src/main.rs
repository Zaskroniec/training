fn main() {
    println!("Hello, world!");

    another_function();

    function_with_int_arg(33);

    let result: i32 = function_with_returned_value();
    println!("result for returned value: {}", result);

    let sum_result: i32 = function_add_number(1, 3);
    println!("sum result: {}", sum_result);
}

fn another_function() {
    println!("another_function");
}

fn function_with_int_arg(number: i32) {
    println!("function argument: {}", number);
}

// statement without semicolon will be returned in function,
// after key_arrow data type needs to be defined
fn function_with_returned_value() -> i32 {
    55
}

fn function_add_number(first_number: i32, second_number: i32) -> i32 {
    first_number - second_number
}
