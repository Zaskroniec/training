﻿using System.Threading.Tasks;
using GraphQL;
using GraphQL.Types;
using Microsoft.AspNetCore.Mvc;
using TrainingAPI.Db.Repositories;
using TrainingAPI.Queries;

namespace TrainingAPI.Controllers
{
    [ApiController]
    [Route("/graphql")]
    public class GraphQLController : ControllerBase
    {
        private readonly IBookingRepository _bookingRepository;

        public GraphQLController(IBookingRepository bookingRepository)
        {
            _bookingRepository = bookingRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] GraphQLQuery query)
        {
            var inputs = query.Variables.ToInputs();
            var schema = new Schema
            {
                Query = new BookingQuery(_bookingRepository)
            };

            var result = await new DocumentExecuter().ExecuteAsync(_ =>
            {
                    _.Schema = schema;
                    _.Query = query.Query;
                    _.OperationName = query.OperationName;
                    _.Inputs = inputs;
            });

            if(result.Errors?.Count > 0)
            {
                return BadRequest();
            }

            return Ok(result);
        }
    }
}
