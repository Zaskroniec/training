﻿using System;
using GraphQL.Types;
using TrainingAPI.Db.Repositories;
using TrainingAPI.Types;

namespace TrainingAPI.Queries
{
    public class BookingQuery : ObjectGraphType
    {
        public BookingQuery(IBookingRepository bookingRepository)
        {
            Field<BookingType>(
                "booking",
                arguments: new QueryArguments(
                    new QueryArgument<IdGraphType> { Name = "id", Description = "The id of the booking" }),
                resolve: context =>
                {
                    var id = context.GetArgument<Guid>("id");
                    var booking = bookingRepository.Get(id);

                    return booking;
                });

            Field<ListGraphType<BookingType>>(
                "bookings",
                resolve: context =>
                {
                    var bookings = bookingRepository.All();

                    return bookings;
                }
            );
        }

    }
}
