﻿using Newtonsoft.Json.Linq;

namespace TrainingAPI.Queries
{
    public class GraphQLQuery
    {
        public string OperationName { set; get; }
        public string NameQuery { set; get; }
        public string Query { set; get; }
        public JObject Variables { get; set; }
    }
}
