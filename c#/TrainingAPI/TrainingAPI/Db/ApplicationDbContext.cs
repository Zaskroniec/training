﻿using Microsoft.EntityFrameworkCore;
using TrainingAPI.Db.Models;

namespace TrainingAPI.Db
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(
            DbContextOptions<ApplicationDbContext> options
        ) : base(options)
        { }

        public DbSet<Booking> Bookings { get; set; }
    }
}
