﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TrainingAPI.Db.Models;

namespace TrainingAPI.Db.Repositories
{
    public class BookingRepository : IBookingRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public BookingRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public async Task<Booking> Get(Guid id)
        {
            return await _applicationDbContext
                .Bookings
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<Booking>> All()
        {
            return await _applicationDbContext
                .Bookings
                .ToListAsync();
        }
    }
}
