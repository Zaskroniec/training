﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TrainingAPI.Db.Models;

namespace TrainingAPI.Db.Repositories
{
    public interface IBookingRepository
    {
        public Task<Booking> Get(Guid id);
        public Task<List<Booking>> All();
    }
}
