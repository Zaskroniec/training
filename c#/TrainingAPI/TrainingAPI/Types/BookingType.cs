﻿using GraphQL.Types;
using TrainingAPI.Db.Models;

namespace TrainingAPI.Types
{
    public class BookingType : ObjectGraphType<Booking>
    {
        public BookingType()
        {
            Name = "booking";

            Field(x => x.Id, type: typeof(IdGraphType)).Description("The id of the booking");
            Field(x => x.Title).Description("The title of the booking");
            Field(x => x.Active).Description("Is it activated");
        }
    }
}
