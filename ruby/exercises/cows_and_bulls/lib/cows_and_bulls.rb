# frozen_string_literal: true

require_relative './cows_and_bulls/constants.rb'

Dir.glob(File.expand_path('./cows_and_bulls/*.rb', __dir__), &method(:require))
