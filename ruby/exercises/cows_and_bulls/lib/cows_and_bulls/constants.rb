# frozen_string_literal: true

module CowsAndBulls
  module Constants
    SECRET_SUPER_NUMBER = 4
    TARGET_RANGE        = 0..9
    STARTING_PROGRESS   = { cows: 0, bulls: 0 }.freeze
    GAME_MESSAGES       = {
      intro:   'Provide 4 digit number code',
      ending:  "#{SECRET_SUPER_NUMBER} cows, congratulations!",
      warning: 'Your input length must be equal:'
    }.freeze
  end
end
