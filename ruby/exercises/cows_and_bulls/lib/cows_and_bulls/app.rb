# frozen_string_literal: true

module CowsAndBulls
  class App
    include Constants

    def initialize(dependencies)
      @kernel      = dependencies.fetch(:kernel)
      @game_engine = dependencies.fetch(:game_engine)
    end

    def call
      display_intro
      start_game
      display_ending
    end

    private

    def start_game
      input = parse_input(kernel.gets)

      unless determine_size(input).eql?(determine_size(game_engine.target_result))
        return raise display_error
      end

      game_engine.(input: input)
      restart_game unless game_engine.current_progress[:cows].eql?(SECRET_SUPER_NUMBER)
    end

    def parse_input(input)
      input.chomp.scan(/\w/).map(&:to_i)
    end

    def restart_game
      display_progress
      game_engine.current_progress = STARTING_PROGRESS.dup
      start_game
    end

    def determine_size(collection)
      collection.size
    end

    def display_intro
      p GAME_MESSAGES[:intro]
    end

    def display_ending
      p GAME_MESSAGES[:ending]
    end

    def display_error
      p "#{GAME_MESSAGES[:warning]} #{game_engine.target_result.size}"
    end

    def display_progress
      p "#{game_engine.current_progress[:cows]} cows, #{game_engine.current_progress[:bulls]} bulls."
    end

    attr_reader :kernel, :game_engine
  end
end
