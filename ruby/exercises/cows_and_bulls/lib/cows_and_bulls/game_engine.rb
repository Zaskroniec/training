# frozen_string_literal: true

module CowsAndBulls
  class GameEngine
    include Constants

    attr_reader :target_result
    attr_accessor :current_progress

    def initialize(target_result: generate_target_result)
      @target_result    = target_result
      @current_progress = STARTING_PROGRESS.dup
    end

    def call(input:)
      target_result.each_with_index do |item, index|
        if input[index].eql?(item)
          current_progress[:cows] += 1
        elsif input.include?(item)
          current_progress[:bulls] += 1
        else
          next
        end
      end
    end

    private

    def generate_target_result
      TARGET_RANGE
        .to_a
        .shuffle!
        .take(SECRET_SUPER_NUMBER)
    end
  end
end
