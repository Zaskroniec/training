# frozen_string_literal: true

require 'spec_helper'

module CowsAndBulls
  describe App do
    describe '#call' do
      let(:kernel) { double(:kernel) }
      let(:dependencies) do
        {
          kernel:      kernel,
          game_engine: game_engine
        }
      end

      subject { described_class.new(dependencies) }

      context 'success' do
        let(:raw_input)   { "1234\n'" }
        let(:input)       { [1, 2, 3, 4] }
        let(:game_engine) { double(:game_engine, current_progress: { cows: 4, bulls: 0 }, target_result: input) }

        before do
          expect(kernel).to receive(:gets).and_return(raw_input)
          expect(game_engine).to receive(:call).with(input: input).and_return(game_engine)
        end

        it 'returns ending message' do
          expect(subject.()).to eq('4 cows, congratulations!')
        end
      end

      context 'failure' do
        context 'invalid input length' do
          let(:raw_input)     { "123\n'" }
          let(:input)         { [1, 2, 3] }
          let(:target_result) { [1, 2, 3, 4] }
          let(:game_engine) do
            double(:game_engine, current_progress: { cows: 3, bulls: 0 }, target_result: target_result)
          end

          before { expect(kernel).to receive(:gets).and_return(raw_input) }

          it 'returns error message' do
            expect { subject.() }.to raise_error(RuntimeError, 'Your input length must be equal: 4')
          end
        end
      end
    end
  end
end
