# frozen_string_literal: true

require 'spec_helper'

module CowsAndBulls
  describe GameEngine do
    describe '#call' do
      let(:target_result) { [1, 2, 3, 4] }

      subject { described_class.new(target_result: target_result) }

      before { subject.(input: input) }

      context 'success' do
        let(:input) { [1, 2, 3, 4] }

        it 'returns 4 cows' do
          expect(subject.current_progress[:cows]).to eq(4)
        end

        it 'returns 0 bulls' do
          expect(subject.current_progress[:bulls]).to eq(0)
        end
      end

      context 'failure' do
        context 'not bad guess' do
          let(:input) { [1, 2, 4, 3] }

          it 'returns 2 cows' do
            expect(subject.current_progress[:cows]).to eq(2)
          end

          it 'returns 2 bulls' do
            expect(subject.current_progress[:bulls]).to eq(2)
          end
        end

        context 'very bad guess' do
          let(:input) { [7, 6, 4, 3] }

          it 'returns 2 cows' do
            expect(subject.current_progress[:cows]).to eq(0)
          end

          it 'returns 2 bulls' do
            expect(subject.current_progress[:bulls]).to eq(2)
          end
        end

        context 'not comment guess...' do
          let(:input) { [9, 9, 9, 9] }

          it 'returns 2 cows' do
            expect(subject.current_progress[:cows]).to eq(0)
          end

          it 'returns 2 bulls' do
            expect(subject.current_progress[:bulls]).to eq(0)
          end
        end
      end
    end
  end
end
