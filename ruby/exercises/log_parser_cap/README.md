# Log parser

## Task description
```shell
Write a ruby script that:
a. Receives a log as argument (webserver.log is provided)
e.g.: ./parser.rb webserver.log
b. Returns the following:
> list of webpages with most page views ordered from most pages views to less page views
e.g.: /home 90 visits /index 80 visits etc… > list of webpages with most unique page views also ordered e.g.: /about/2 8 unique views
/index 5 unique views etc…
```

## Requirements

* Ruby **2.6.5**

## Instalation

* Run `bundle install`
* Run in your shell `./bin/app`

## Tests

* Run `rspec` to verify specs in app
