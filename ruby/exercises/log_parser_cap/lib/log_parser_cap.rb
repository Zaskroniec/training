# frozen_string_literal: true

require 'active_support/core_ext/object/blank'
require_relative './log_parser_cap/constants.rb'

Dir.glob(File.expand_path('./log_parser_cap/*.rb', __dir__), &method(:require))
Dir.glob(File.expand_path('./log_parser_cap/services/*.rb', __dir__), &method(:require))
Dir.glob(File.expand_path('./log_parser_cap/formatters/*.rb', __dir__), &method(:require))
