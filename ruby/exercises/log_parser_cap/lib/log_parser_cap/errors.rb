# frozen_string_literal: true

module LogParserCap
  class Errors < StandardError; end
  class ProcessError < StandardError; end
end
