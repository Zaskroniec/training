# frozen_string_literal: true

module LogParserCap
  class App
    include Constants

    def initialize(dependencies)
      @file_parser   = dependencies.fetch(:file_parser)
      @file_client   = dependencies.fetch(:file_client)
      @log_formatter = dependencies.fetch(:log_formatter)
    end

    def call(file_path:)
      raise ProcessError, 'Missing path to file.' if file_path.blank?

      file_location = file_client.expand_path(file_path)

      raise ProcessError, 'Could not find file.' unless file_client.exist?(file_location)

      file_client
        .read(file_location)
        .split("\n")
        .then { |raw_data| file_parser.(raw_data: raw_data) }
        .group_by { |hash| hash[:path] }
        .then(&method(:segregate_groups))
    end

    private

    def segregate_groups(groups)
      first_scenario  = groups.map { |item| [item[WEB_PATH], item[WEB_IP].size] }.sort_by { |item| -item[WEB_IP] }
      second_scenario = groups.map { |item| [item[WEB_PATH], item[WEB_IP].uniq.size] }.sort_by { |item| -item[WEB_IP] }

      result(first_scenario, second_scenario)
    end

    def result(first_scenario, second_scenario)
      [
        {
          name:       'List of webpages with most pages views order from most pages views to lesss page views',
          collection: first_scenario,
          pretty:     log_formatter.(collection: first_scenario)
        },
        {
          name:       'List of webpages with most unique pages views',
          collection: second_scenario,
          pretty:     log_formatter.(collection: second_scenario, type: :uniq)
        }
      ]
    end

    attr_reader :file_parser, :file_client, :log_formatter
  end
end
