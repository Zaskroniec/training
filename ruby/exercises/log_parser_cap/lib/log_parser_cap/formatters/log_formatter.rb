# frozen_string_literal: true

module LogParserCap
  module Formatters
    class LogFormatter
      include Constants

      def call(collection:, type: :normal)
        collection
          .map { |item_array| parse_item(item_array, type)}
          .join("\n")
      end

      private

      def parse_item(item_array, type)
        item_array
          .join(' ')
          .then { |string| "#{string} #{FORMATTER_TYPES[type]}" }
      end
    end
  end
end
