# frozen_string_literal: true

module LogParserCap
  module Services
    class FileParser
      include Constants

      def call(raw_data:)
        raw_data.map do |string|
          string
            .split(' ')
            .then { |items| { path: items[WEB_PATH], user_ip: items[WEB_IP] } }
        end
      end
    end
  end
end
