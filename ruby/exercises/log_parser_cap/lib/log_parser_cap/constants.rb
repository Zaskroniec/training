# frozen_string_literal: true

module LogParserCap
  module Constants
    WEB_PATH = 0
    WEB_IP   = 1

    FORMATTER_TYPES = {
      uniq:   'uniqe views',
      normal: 'visits'
    }.freeze
  end
end
