# frozen_string_literal: true

# rubocop:disable Layout/IndentationWidth
def formatted_pretty_normal_string
"/about/2 90 visits
/contact 89 visits
/index 82 visits
/about 81 visits
/help_page/1 80 visits
/home 78 visits"
end

def formatted_pretty_uniq_string
"/about/2 90 uniqe views
/contact 89 uniqe views
/index 82 uniqe views
/about 81 uniqe views
/help_page/1 80 uniqe views
/home 78 uniqe views"
end

def pretty_first_scenario
"/help_page/1 3 visits
/contact 1 visits"
end

def pretty_second_scenario
"/help_page/1 2 uniqe views
/contact 1 uniqe views"
end
# rubocop:enable Layout/IndentationWidth

def raw_file
  "/help_page/1 126.318.035.038\n/help_page/1 126.318.035.038\n/contact 184.123.665.067\n/help_page/1 929.398.951.889\n"
end

def splitted_data
  [
    '/help_page/1 126.318.035.038', '/help_page/1 126.318.035.038',
    '/contact 184.123.665.067', '/help_page/1 929.398.951.889'
  ]
end

def parsed_data
  [
    { path: '/help_page/1', user_ip: '126.318.035.038' },
    { path: '/help_page/1', user_ip: '126.318.035.038' },
    { path: '/contact', user_ip: '184.123.665.067' },
    { path: '/help_page/1', user_ip: '929.398.951.889' }
  ]
end

def first_scenario
  [['/help_page/1', 3], ['/contact', 1]]
end

def second_scenario
  [['/help_page/1', 2], ['/contact', 1]]
end
