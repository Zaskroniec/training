# frozen_string_literal: true

require 'spec_helper'

module LogParserCap
  describe App do
    describe '#call' do
      let(:file_parser)   { double(:file_parser) }
      let(:file_client)   { double(:file_client) }
      let(:log_formatter) { double(:log_formatter) }
      let(:file_path)     { 'text.txt' }
      let(:file_location) { "/SomePath/#{file_path}" }
      let(:dependencies) do
        {
          file_parser:   file_parser,
          file_client:   file_client,
          log_formatter: log_formatter
        }
      end

      context 'success' do
        let(:expected_result) do
          [
            {
              name:       'List of webpages with most pages views order from most pages views to lesss page views',
              collection: first_scenario,
              pretty:     pretty_first_scenario
            },
            {
              name:       'List of webpages with most unique pages views',
              collection: second_scenario,
              pretty:     pretty_second_scenario
            }
          ]
        end
        subject { described_class.new(dependencies).(file_path: file_path) }

        before do
          expect(file_client).to receive(:expand_path).with(file_path).and_return(file_location)
          expect(file_client).to receive(:exist?).with(file_location).and_return(true)
          expect(file_client).to receive(:read).with(file_location).and_return(raw_file)
          expect(file_parser).to receive(:call).with(raw_data: splitted_data).and_return(parsed_data)
          expect(log_formatter)
            .to receive(:call).with(collection: first_scenario).and_return(pretty_first_scenario)
          expect(log_formatter)
            .to receive(:call).with(collection: second_scenario, type: :uniq).and_return(pretty_second_scenario)
        end

        it 'returns segregated groups' do
          expect(subject).to eq(expected_result)
        end
      end

      context 'failure' do
        context 'missing file_path' do
          subject { described_class.new(dependencies).(file_path: nil) }

          it 'raises error' do
            expect { subject }.to raise_error(ProcessError, 'Missing path to file.')
          end
        end

        context 'file not exists' do
          subject { described_class.new(dependencies).(file_path: file_path) }

          before do
            expect(file_client).to receive(:expand_path).with(file_path).and_return(file_location)
            expect(file_client).to receive(:exist?).with(file_location).and_return(false)
          end

          it 'raises error' do
            expect { subject }.to raise_error(ProcessError, 'Could not find file.')
          end
        end
      end
    end
  end
end
