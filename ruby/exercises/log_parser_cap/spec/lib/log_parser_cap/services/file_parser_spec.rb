# frozen_string_literal: true

require 'spec_helper'

module LogParserCap
  module Services
    describe FileParser do
      describe '#call' do
        let(:raw_data) do
          ['/help_page/1 126.318.035.038', '/contact 184.123.665.067']
        end
        let(:expected_result) do
          [
            { path: '/help_page/1', user_ip: '126.318.035.038' },
            { path: '/contact', user_ip: '184.123.665.067' }
          ]
        end

        subject { described_class.new.(raw_data: raw_data) }

        it 'returns valid parsed collection' do
          expect(subject).to eq(expected_result)
        end
      end
    end
  end
end
