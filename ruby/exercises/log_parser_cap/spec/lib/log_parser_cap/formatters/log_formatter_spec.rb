# frozen_string_literal: true

require 'spec_helper'

module LogParserCap
  module Formatters
    describe LogFormatter do
      describe '#call' do
        let(:collection) do
          [
            ['/about/2', 90], ['/contact', 89],
            ['/index', 82], ['/about', 81],
            ['/help_page/1', 80], ['/home', 78]
          ]
        end

        context 'normal format' do
          let(:expected_result) { formatted_pretty_normal_string }

          subject { described_class.new.(collection: collection) }

          it 'returns formatted as normal collection' do
            expect(subject).to eq(expected_result)
          end
        end

        context 'uniq format' do
          let(:expected_result) { formatted_pretty_uniq_string }

          subject { described_class.new.(collection: collection, type: :uniq) }

          it 'returns formatted as uniq collection' do
            expect(subject).to eq(expected_result)
          end
        end
      end
    end
  end
end
