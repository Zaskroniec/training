# frozen_string_literal: true

Rails.application.routes.draw do
  scope module: :api, defaults: { format: :json } do
    namespace :v1 do
      resources :users, only: %i[create destroy] do
        get :courses, on: :member
      end
      resources :courses, only: %i[index create destroy] do
        member do
          post :enroll
          delete :withdraw
        end
      end
    end
  end
end
