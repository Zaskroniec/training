# README

## Info

* Ruby version 2.6.5
* Rails version 6.0.1
* PostgreSQL version 11
* <span style="color:red;font-weight:700;">Important:</span> **Master key available in ./config/** (don't use such move in private repos :))

## Development Setup

* Run `docker-compose -f docker-compose.development.yml build`
* Run `docker-compose -f docker-compose.development.yml up -d`
* Run `docker-compose -f docker-compose.development.yml run --rm api bundle exec rake db:setup`

## Additional info
* To check specs run `docker-compose -f docker-compose.development.yml run --rm api bundle exec rspec spec`
* To check app credentials `docker-compose -f docker-compose.development.yml run --rm api bundle exec rails credentials:edit`
* Run guard by `docker-compose -f docker-compose.development.yml run --rm api bundle exec guard`

## API endpoints

### User Resource

#### Create User
<span style="color:yellow;">**POST** /v1/users</span>

Request:
```json
{
  "user": {
    "email": "test@test.com",
  }
}
```

Response:
```json
{
  "data": {
    "id": 8,
    "email": "test@test.com",
    "created_at": "2019-12-29T22:42:33.973Z",
    "updated_at": "2019-12-29T22:42:33.973Z"
  },
  "message": "Successfully created new user."
}
```
---

#### Destroy User
<span style="color:yellow;">**DELETE** /v1/users/{ID}</span>

Response
```json
{
  "message": "Successfully destroyed user."
}
```
---

#### User courses
<span style="color:yellow;">**GET** /v1/users/{ID}/courses</span>

Response
```json
{
  "data": [
    {
      "id": 77,
      "name": "Title",
      "enrollments_count": 1,
      "created_at": "2019-12-14T22:55:58.608Z",
      "updated_at": "2019-12-14T22:55:58.608Z"
    }
  ]
}
```
---


### Course Resource

#### Create Course
<span style="color:yellow;">**POST** /v1/courses</span>

Request:
```json
{
  "course": {
    "name": "Title",
  }
}
```

Response:
```json
{
  "data": {
    "id": 14,
    "name": "Title",
    "enrollments_count": 0,
    "created_at": "2019-12-14T22:55:58.608Z",
    "updated_at": "2019-12-14T22:55:58.608Z"
  },
  "message": "Successfully created new course."
}
```
---

#### Destroy Course
<span style="color:yellow;">**DELETE** /v1/courses/{ID}</span>

Response
```json
{
  "message": "Successfully destroyed course."
}
```
---

#### Courses with enrollments
<span style="color:yellow;">**GET** /v1/courses</span>

Response
```json
{
  "data": [
    {
      "id": 13,
      "name": "Title",
      "enrollments_count": 0
    },
    {
      "id": 12,
      "name": "Test course #2",
      "enrollments_count": 1
    },
    {
      "id": 11,
      "name": "Test course #1",
      "enrollments_count": 1
    }
  ]
}
```
---

#### Enroll to Course
<span style="color:yellow;">**POST** /v1/courses/{ID}/enroll</span>

Request:
```json
{
  "user_id": 10
}
```

Response:
```json
{
  "message": "Successfully enrolled to course Title."
}
```
---

#### Withdraw from Course
<span style="color:yellow;">**DELETE** /v1/courses/{ID}/withdraw</span>

Request:
```json
{
  "user_id": 10
}
```

Response:
```json
{
  "message": "Successfully withdrawn from course Title."
}
```
---
