# frozen_string_literal: true

def time_now
  Time.now.utc
end
