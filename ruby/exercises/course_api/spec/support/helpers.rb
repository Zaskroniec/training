# frozen_string_literal: true

def parsed_respose_body
  JSON.parse(response.body, symbolize_names: true)
end

def string_hash(hash)
  hash.stringify_keys
end
