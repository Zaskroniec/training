# frozen_string_literal: true

shared_examples :http_response_without_data do
  it 'returns empty data param' do
    expect(parsed_respose_body[:data]).to be_blank
  end
end

shared_examples :http_response_with_data do
  it 'returns empty data param' do
    expect(parsed_respose_body[:data]).to be_present
  end
end

shared_examples :http_valid_status do |target_status:|
  it 'returns valid status' do
    expect(status).to eq(target_status)
  end
end

shared_examples :http_valid_message do |target_message:|
  it 'returns valid status' do
    expect(parsed_respose_body[:message]).to eq(target_message)
  end
end

shared_examples :http_no_message_param do
  it 'returns no message' do
    expect(parsed_respose_body[:message]).to be_blank
  end
end

shared_examples :http_record_not_found do |resource_name:|
  it_behaves_like :http_valid_status, target_status: 404
  it_behaves_like :http_valid_message, target_message: "Could not find #{resource_name}."
  it_behaves_like :http_response_without_data
end
