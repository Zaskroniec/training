# frozen_string_literal: true

%i[users courses enrollments].each do |type|
  define_method "#{type}_repo_mock" do
    double(:"#{type}_repo")
  end
end

%i[user course enrollment].each do |type|
  define_method "#{type}_mock" do |options: {}|
    double(type, **options)
  end
end
