# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
  end

  factory :user_with_courses, parent: :user do
    after :create do |user|
      user.courses.create(name: 'Test course #1')
      user.courses.create(name: 'Test course #2')
    end
  end
end
