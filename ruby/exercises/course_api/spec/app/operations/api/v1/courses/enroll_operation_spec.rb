# frozen_string_literal: true

require 'rails_helper'

module API
  module V1
    module Courses
      describe EnrollOperation do
        describe '#call' do
          context 'sucess' do
            let(:course)          { course_mock(options: { id: 1, name: Faker::Book.title }) }
            let(:user)            { user_mock(options: { id: 1 }) }
            let(:enrollment)      { enrollment_mock }
            let(:enrollment_repo) { enrollments_repo_mock }

            subject { described_class.new.(course: course, user: user) }

            before do
              expect(course).to receive(:enrollments).and_return(enrollment_repo)
              expect(enrollment_repo).to receive(:create).and_return(enrollment)
            end

            it 'returns valid message' do
              expect(subject.success[:message]).to eq("Successfully enrolled to course #{course.name}.")
            end

            it 'returns valid status' do
              expect(subject.success[:status]).to eq(:created)
            end
          end
        end
      end
    end
  end
end
