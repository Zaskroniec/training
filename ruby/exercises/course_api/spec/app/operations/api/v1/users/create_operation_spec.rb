# frozen_string_literal: true

require 'rails_helper'

module API
  module V1
    module Users
      describe CreateOperation do
        let(:dependencies) do
          {
            users_repo:          users_repo_mock,
            user_data_validator: API::V1::Users::UserDataValidator.new
          }
        end

        subject { described_class.new(dependencies).(user_params: user_params) }

        context 'success' do
          let(:user_params) { { email: Faker::Internet.email } }
          let(:user_hash) do
            user_params.merge(
              id:         1,
              created_at: time_now,
              updated_at: time_now
            )
          end
          let(:new_user) do
            user_mock(options: user_hash)
          end

          before { expect(dependencies[:users_repo]).to receive(:create).with(user_params).and_return(new_user) }

          it 'returns user data' do
            expect(subject.success[:data]).to eq(new_user)
          end

          it 'returns valid message' do
            expect(subject.success[:message]).to eq('Successfully created new user.')
          end

          it 'returns valid status' do
            expect(subject.success[:status]).to eq(:created)
          end
        end

        context 'failure' do
          context 'validation errors' do
            let(:user_params) { { email: 1 } }

            it 'returns valid error message for email' do
              expect(subject.failure[:message][:email]).to eq(['must be a string'])
            end

            it 'returns valid status' do
              expect(subject.failure[:status]).to eq(:unprocessable_entity)
            end
          end
        end
      end
    end
  end
end
