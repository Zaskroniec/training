# frozen_string_literal: true

require 'rails_helper'

module API
  module V1
    module Courses
      describe DestroyOperation do
        let(:course) { course_mock }

        subject { described_class.new.(course: course) }

        context 'success' do
          before { expect(course).to receive(:destroy).and_return(true) }

          it 'returns valid message' do
            expect(subject.success[:message]).to eq('Successfully destroyed course.')
          end

          it 'returns valid status' do
            expect(subject.success[:status]).to eq(:ok)
          end
        end

        context 'failure' do
          before { expect(course).to receive(:destroy).and_return(false) }

          it 'returns valid message' do
            expect(subject.failure[:message]).to eq('Something went wrong. Sorry m8!')
          end

          it 'returns valid status' do
            expect(subject.failure[:status]).to eq(:unprocessable_entity)
          end
        end
      end
    end
  end
end
