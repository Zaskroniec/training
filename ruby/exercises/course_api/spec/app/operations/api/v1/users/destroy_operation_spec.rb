# frozen_string_literal: true

require 'rails_helper'

module API
  module V1
    module Users
      describe DestroyOperation do
        let(:user) { user_mock }

        subject { described_class.new.(user: user) }

        context 'success' do
          before { expect(user).to receive(:destroy).and_return(true) }

          it 'returns valid message' do
            expect(subject.success[:message]).to eq('Successfully destroyed user.')
          end

          it 'returns valid status' do
            expect(subject.success[:status]).to eq(:ok)
          end
        end

        context 'failure' do
          before { expect(user).to receive(:destroy).and_return(false) }

          it 'returns valid message' do
            expect(subject.failure[:message]).to eq('Something went wrong. Sorry m8!')
          end

          it 'returns valid status' do
            expect(subject.failure[:status]).to eq(:unprocessable_entity)
          end
        end
      end
    end
  end
end
