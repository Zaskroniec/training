# frozen_string_literal: true

require 'rails_helper'

module API
  module V1
    module Courses
      describe CreateOperation do
        let(:dependencies) do
          {
            courses_repo:          courses_repo_mock,
            course_data_validator: API::V1::Courses::CourseDataValidator.new
          }
        end

        subject { described_class.new(dependencies).(course_params: course_params) }

        context 'success' do
          let(:course_params) { { name: Faker::Book.title } }
          let(:course_hash) do
            course_params.merge(
              id:                1,
              enrollments_count: 0,
              created_at:        time_now,
              updated_at:        time_now
            )
          end
          let(:new_course) { course_mock(options: course_hash) }

          before { expect(dependencies[:courses_repo]).to receive(:create).with(course_params).and_return(new_course) }

          it 'returns course data' do
            expect(subject.success[:data]).to eq(new_course)
          end

          it 'returns valid message' do
            expect(subject.success[:message]).to eq('Successfully created new course.')
          end

          it 'returns valid status' do
            expect(subject.success[:status]).to eq(:created)
          end
        end

        context 'failure' do
          context 'validation errors' do
            let(:course_params) { { name: 1 } }

            it 'returns valid error message for title' do
              expect(subject.failure[:message][:name]).to eq(['must be a string'])
            end

            it 'returns valid status' do
              expect(subject.failure[:status]).to eq(:unprocessable_entity)
            end
          end
        end
      end
    end
  end
end
