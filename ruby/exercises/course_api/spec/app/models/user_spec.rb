# frozen_string_literal: true

require 'rails_helper'

describe User, type: :model do
  it { is_expected.to have_many(:enrollments) }
  it { is_expected.to have_many(:courses).through(:enrollments) }

  it { is_expected.to have_db_column(:email).of_type(:string).with_options(null: false) }
  it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
end
