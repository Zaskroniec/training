# frozen_string_literal: true

require 'rails_helper'

describe Enrollment, type: :model do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:course).counter_cache(true) }

  it { is_expected.to have_db_column(:user_id).of_type(:integer).with_options(null: false, foreign_key: true) }
  it { is_expected.to have_db_column(:course_id).of_type(:integer).with_options(null: false, foreign_key: true) }
  it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
end
