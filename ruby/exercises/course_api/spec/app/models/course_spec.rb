# frozen_string_literal: true

require 'rails_helper'

describe Course, type: :model do
  it { is_expected.to have_many(:enrollments) }
  it { is_expected.to have_many(:users).through(:enrollments) }

  it { is_expected.to have_db_column(:name).of_type(:string).with_options(null: false) }
  it { is_expected.to have_db_column(:enrollments_count).of_type(:integer).with_options(default: 0) }
  it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
end
