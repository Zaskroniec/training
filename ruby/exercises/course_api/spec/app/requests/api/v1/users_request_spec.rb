# frozen_string_literal: true

require 'rails_helper'

module API
  module V1
    describe UsersController, type: :request do
      describe '#create' do
        before { post '/v1/users', params: params }

        context 'success' do
          let(:params) { { user: { email: Faker::Internet.email } } }

          it_behaves_like :http_valid_status, target_status: 201
          it_behaves_like :http_valid_message, target_message: 'Successfully created new user.'
          it_behaves_like :http_response_with_data
        end

        context 'failure' do
          let(:params) { { user: {} } }

          it_behaves_like :http_valid_status, target_status: 422
          it_behaves_like :http_valid_message, target_message: { email: ['is missing'] }
          it_behaves_like :http_response_without_data
        end
      end

      describe '#destroy' do
        before { delete "/v1/users/#{user.id}" }

        context 'success' do
          let(:user) { create(:user) }

          it_behaves_like :http_valid_status, target_status: 200
          it_behaves_like :http_valid_message, target_message: 'Successfully destroyed user.'
          it_behaves_like :http_response_without_data
        end

        context 'failure' do
          let(:user) { user_mock(options: { id: 10 }) }

          it_behaves_like :http_record_not_found, resource_name: 'User'
        end
      end

      describe '#courses' do
        before { get "/v1/users/#{user.id}/courses" }

        context 'success' do
          let!(:user) { create(:user_with_courses) }

          it_behaves_like :http_valid_status, target_status: 200
          it_behaves_like :http_no_message_param
          it_behaves_like :http_response_with_data

          it 'returns collection of valid records' do
            expect(parsed_respose_body[:data][0][:name]).to eq('Test course #2')
            expect(parsed_respose_body[:data][1][:name]).to eq('Test course #1')
          end
        end

        context 'failure' do
          let(:user) { user_mock(options: { id: 10 }) }

          it_behaves_like :http_record_not_found, resource_name: 'User'
        end
      end
    end
  end
end
