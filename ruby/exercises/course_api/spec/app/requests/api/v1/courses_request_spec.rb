# frozen_string_literal: true

require 'rails_helper'

module API
  module V1
    describe CoursesController, type: :request do
      describe '#index' do
        before do
          2.times { |i| create(:course, name: "test_#{i}") }

          get '/v1/courses'
        end

        it_behaves_like :http_valid_status, target_status: 200
        it_behaves_like :http_no_message_param
        it_behaves_like :http_response_with_data

        it 'returns collection of valid record' do
          expect(parsed_respose_body[:data][0][:name]).to eq('test_1')
          expect(parsed_respose_body[:data][1][:name]).to eq('test_0')
        end
      end

      describe '#create' do
        before { post '/v1/courses', params: params }

        context 'success' do
          let(:params) { { course: { name: Faker::Book.title } } }

          it_behaves_like :http_valid_status, target_status: 201
          it_behaves_like :http_valid_message, target_message: 'Successfully created new course.'
          it_behaves_like :http_response_with_data
        end

        context 'failure' do
          let(:params) { { course: {} } }

          it_behaves_like :http_valid_status, target_status: 422
          it_behaves_like :http_valid_message, target_message: { name: ['is missing'] }
          it_behaves_like :http_response_without_data
        end
      end

      describe '#destroy' do
        before { delete "/v1/courses/#{course.id}" }

        context 'success' do
          let(:course) { create(:course) }

          it_behaves_like :http_valid_status, target_status: 200
          it_behaves_like :http_valid_message, target_message: 'Successfully destroyed course.'
          it_behaves_like :http_response_without_data
        end

        context 'failure' do
          let(:course) { course_mock(options: { id: 10 }) }

          it_behaves_like :http_record_not_found, resource_name: 'Course'
        end
      end

      describe '#enroll' do
        let!(:course) { create(:course, name: 'test') }
        let!(:user)   { create(:user) }
        let(:params)  { { user_id: user.id } }

        before { post "/v1/courses/#{course.id}/enroll", params: params }

        context 'success' do
          it_behaves_like :http_valid_status, target_status: 201
          it_behaves_like :http_valid_message, target_message: 'Successfully enrolled to course test.'
          it_behaves_like :http_response_without_data
        end

        context 'failure' do
          context 'invalid course_id' do
            let(:course) { course_mock(options: { id: 10 }) }

            it_behaves_like :http_record_not_found, resource_name: 'Course'
          end

          context 'invalid user_id' do
            let!(:course) { create(:course) }
            let(:user)    { user_mock(options: { id: 10 }) }
            let(:params)  { { user_id: user.id } }

            it_behaves_like :http_record_not_found, resource_name: 'User'
          end

          context 'missing user_id' do
            let!(:course) { create(:course) }
            let(:params)  { {} }

            it_behaves_like :http_record_not_found, resource_name: 'User'
          end
        end
      end

      describe '#withdraw' do
        let!(:user)  { create(:user_with_courses) }
        let(:course) { user.courses.first }
        let(:params) { { user_id: user.id } }

        before { delete "/v1/courses/#{course.id}/withdraw", params: params }

        context 'success' do
          it_behaves_like :http_valid_status, target_status: 200
          it_behaves_like :http_valid_message, target_message: 'Successfully withdrawn from course Test course #1.'
          it_behaves_like :http_response_without_data
        end

        context 'failure' do
          before { delete "/v1/courses/#{course.id}/withdraw", params: params }

          context 'invalid course_id' do
            let(:course) { course_mock(options: { id: 10 }) }

            it_behaves_like :http_record_not_found, resource_name: 'Course'
          end

          context 'invalid user_id' do
            let!(:course) { create(:course) }
            let(:user)    { user_mock(options: { id: 10 }) }
            let(:params)  { { user_id: user.id } }

            it_behaves_like :http_record_not_found, resource_name: 'Enrollment'
          end

          context 'missing user_id' do
            let!(:course) { create(:course) }
            let(:params)  { {} }

            it_behaves_like :http_record_not_found, resource_name: 'Enrollment'
          end
        end
      end
    end
  end
end
