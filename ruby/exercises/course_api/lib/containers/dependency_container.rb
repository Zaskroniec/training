# frozen_string_literal: true

module Containers
  class DependencyContainer
    extend Dry::Container::Mixin

    namespace(:operations) do
      namespace(:api) do
        namespace(:v1) do
          namespace(:users) do
            register(:create_operation) { API::V1::Users::CreateOperation.new }
            register(:destroy_operation) { API::V1::Users::DestroyOperation.new }
          end

          namespace(:courses) do
            register(:create_operation) { API::V1::Courses::CreateOperation.new }
            register(:destroy_operation) { API::V1::Courses::DestroyOperation.new }
            register(:enroll_operation) { API::V1::Courses::EnrollOperation.new }
            register(:withdraw_operation) { API::V1::Courses::WithdrawOperation.new }
          end
        end
      end
    end

    namespace(:serializers) do
      namespace(:api) do
        namespace(:v1) do
          namespace(:courses) do
            register(:course_serializer) { API::V1::Courses::CourseSerializer.new }
          end
        end
      end
    end

    namespace(:validators) do
      namespace(:api) do
        namespace(:v1) do
          namespace(:users) do
            register(:user_data_validator) { API::V1::Users::UserDataValidator.new }
          end

          namespace(:courses) do
            register(:course_data_validator) { API::V1::Courses::CourseDataValidator.new }
          end
        end
      end
    end

    namespace(:repositories) do
      register(:users_repo) { User }
      register(:courses_repo) { Course }
      register(:enrollments_repo) { Enrollment }
    end

    namespace(:globals) do
      register(:i18n) { I18n }
    end
  end
end
