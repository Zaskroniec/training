# frozen_string_literal: true

DependencyImporter = Dry::AutoInject(Containers::DependencyContainer)
