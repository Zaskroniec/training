# frozen_string_literal: true

class ApplicationValidator < Dry::Validation::Contract
  config.messages.backend = :i18n
  config.messages.default_locale = I18n.default_locale
end
