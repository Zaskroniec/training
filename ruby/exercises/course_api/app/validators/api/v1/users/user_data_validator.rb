# frozen_string_literal: true

module API
  module V1
    module Users
      class UserDataValidator < ApplicationValidator
        json do
          required(:email).value(:string)
        end
      end
    end
  end
end
