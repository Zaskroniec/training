# frozen_string_literal: true

module API
  module V1
    module Courses
      class CourseDataValidator < ApplicationValidator
        json do
          required(:name).value(:string)
        end
      end
    end
  end
end
