# frozen_string_literal: true

class Course < ApplicationRecord
  has_many :enrollments, dependent: :destroy
  has_many :users, through: :enrollments

  scope :newest, -> { order(created_at: :desc) }
end
