# frozen_string_literal: true

module API
  module V1
    module Courses
      class CourseSerializer
        COURSE_ATTRIBUTES = %w[id name enrollments_count].freeze

        # Intention of this "super simple" serializer was to use
        # custom serialization instead using 3rd gems like active_model_serializers, fast_jsonapi or others ;)
        def call(object:)
          object
            .serializable_hash
            .slice(*COURSE_ATTRIBUTES)
        end
      end
    end
  end
end
