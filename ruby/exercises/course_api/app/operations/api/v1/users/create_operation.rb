# frozen_string_literal: true

module API
  module V1
    module Users
      class CreateOperation
        include Dry::Monads[:result]
        include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
        include DependencyImporter[
          'repositories.users_repo',
          'validators.api.v1.users.user_data_validator',
          'globals.i18n'
        ]

        def call(user_params:)
          validate_params(user_params)
            .bind(&method(:create_user))
        end

        private

        def validate_params(user_params)
          validator = user_data_validator.(user_params)

          unless validator.success?
            return Failure(
              message: validator.errors.to_h,
              status:  :unprocessable_entity
            )
          end

          Success(validator.to_h)
        end

        def create_user(data)
          user = users_repo.create(data)

          Success(
            data:    user,
            message: i18n.t('notices.users.create'),
            status:  :created
          )
        end
      end
    end
  end
end
