# frozen_string_literal: true

module API
  module V1
    module Users
      class DestroyOperation
        include Dry::Monads[:result]
        include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
        include DependencyImporter['globals.i18n']

        def call(user:)
          return success_result if user.destroy

          failure_result
        end

        private

        def success_result
          Success(
            message: i18n.t('notices.users.destroy'),
            status:  :ok
          )
        end

        def failure_result
          Failure(
            message: i18n.t('errors.something'),
            status:  :unprocessable_entity
          )
        end
      end
    end
  end
end
