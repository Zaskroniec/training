# frozen_string_literal: true

module API
  module V1
    module Courses
      class EnrollOperation
        include Dry::Monads[:result]
        include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
        include DependencyImporter['globals.i18n']

        def call(course:, user:)
          course
            .enrollments
            .create(user: user)

          Success(
            message: i18n.t('notices.courses.enroll', name: course.name),
            status:  :created
          )
        end
      end
    end
  end
end
