# frozen_string_literal: true

module API
  module V1
    module Courses
      class CreateOperation
        include Dry::Monads[:result]
        include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
        include DependencyImporter[
          'repositories.courses_repo',
          'validators.api.v1.courses.course_data_validator',
          'globals.i18n'
        ]

        def call(course_params:)
          validate_params(course_params)
            .bind(&method(:create_course))
        end

        private

        def validate_params(course_params)
          validator = course_data_validator.(course_params)

          unless validator.success?
            return Failure(
              message: validator.errors.to_h,
              status:  :unprocessable_entity
            )
          end

          Success(validator.to_h)
        end

        def create_course(data)
          course = courses_repo.create(data)

          Success(
            data:    course,
            message: i18n.t('notices.courses.create'),
            status:  :created
          )
        end
      end
    end
  end
end
