# frozen_string_literal: true

module API
  module V1
    module Courses
      class WithdrawOperation
        include Dry::Monads[:result]
        include Dry::Matcher.for(:call, with: Dry::Matcher::ResultMatcher)
        include DependencyImporter['globals.i18n']

        def call(course:, enrollment:)
          return success_result(course) if enrollment.destroy

          failure_result
        end

        private

        def success_result(course)
          Success(
            message: i18n.t('notices.courses.withdraw', name: course.name),
            status:  :ok
          )
        end

        def failure_result
          Failure(
            message: i18n.t('errors.something'),
            status:  :unprocessable_entity
          )
        end
      end
    end
  end
end
