# frozen_string_literal: true

class ApplicationController < ActionController::API
  include APIErrors

  private

  def unsafe_params
    params.to_unsafe_h.deep_symbolize_keys
  end

  def render_response(**args)
    render json: build_response_body(args), status: args.fetch(:status)
  end

  def build_response_body(**args)
    args.slice(:data, :message).tap do |body|
      map_data(body, args) if body[:data].present?
    end
  end

  def map_data(body_hash, **args)
    body_hash[:data] = if args[:each_serializer].present?
                         body_hash[:data].map { |item| args[:each_serializer].(object: item) }
                       elsif args[:serializer].present?
                         args[:serializer].(object: body_hash[:data])
                       else
                         body_hash[:data]
                       end
  end
end
