# frozen_string_literal: true

module API
  module V1
    class CoursesController < ApplicationController
      include DependencyImporter[
        'operations.api.v1.courses.create_operation',
        'operations.api.v1.courses.destroy_operation',
        'operations.api.v1.courses.enroll_operation',
        'operations.api.v1.courses.withdraw_operation',
        'serializers.api.v1.courses.course_serializer',
        'repositories.courses_repo',
        'repositories.users_repo'
      ]

      def index
        render_response(data: courses_repo.newest, status: :ok, each_serializer: course_serializer)
      end

      def create
        create_operation.(course_params: unsafe_params[:course]) do |service|
          service.success(&method(:render_response))
          service.failure(&method(:render_response))
        end
      end

      def destroy
        destroy_operation.(course: course) do |service|
          service.success(&method(:render_response))
          service.failure(&method(:render_response))
        end
      end

      def enroll
        enroll_operation.(course: course, user: user) do |service|
          service.success(&method(:render_response))
          service.failure { |_result| }
        end
      end

      def withdraw
        withdraw_operation.(course: course, enrollment: enrollment) do |service|
          service.success(&method(:render_response))
          service.failure(&method(:render_response))
        end
      end

      private

      def course
        @course ||= courses_repo.find(unsafe_params[:id])
      end

      def user
        @user ||= users_repo.find(unsafe_params[:user_id])
      end

      def enrollment
        @enrollment ||= course.enrollments.find_by!(user_id: unsafe_params[:user_id])
      end
    end
  end
end
