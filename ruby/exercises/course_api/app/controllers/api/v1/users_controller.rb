# frozen_string_literal: true

module API
  module V1
    class UsersController < ApplicationController
      include DependencyImporter[
        'operations.api.v1.users.create_operation',
        'operations.api.v1.users.destroy_operation',
        'repositories.users_repo'
      ]

      def create
        create_operation.(user_params: unsafe_params[:user]) do |service|
          service.success(&method(:render_response))
          service.failure(&method(:render_response))
        end
      end

      def destroy
        destroy_operation.(user: user) do |service|
          service.success(&method(:render_response))
          service.failure(&method(:render_response))
        end
      end

      def courses
        render_response(data: user.courses.newest, status: :ok)
      end

      private

      def user
        @user ||= users_repo.find(unsafe_params[:id])
      end
    end
  end
end
