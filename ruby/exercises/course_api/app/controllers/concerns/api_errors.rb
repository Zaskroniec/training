# frozen_string_literal: true

module APIErrors
  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
    rescue_from ActiveRecord::RecordInvalid, with: :record_invalid

    private

    def record_not_found(exception)
      render_response(
        message: I18n.t('errors.not_found', resource: exception.model),
        status:  :not_found
      )
    end

    def record_invalid(exception)
      render_response(
        message: I18n.t('errors.record_invalid', resource: exception.model),
        status:  :unprocessable_entity
      )
    end
  end
end
