# frozen_string_literal: true

class CreateCourses < ActiveRecord::Migration[6.0]
  def change
    create_table :courses do |t|
      t.string  :name, null: false
      t.integer :enrollments_count, default: 0

      t.timestamps
    end
  end
end
