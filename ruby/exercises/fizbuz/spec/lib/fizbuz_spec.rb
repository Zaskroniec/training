require 'spec_helper'

describe FizBuz do
  describe '#call' do
    subject { described_class.new.() }

    it 'returns 1 time counted fizbuz' do
      expect(subject[:fizbuz]).to eq(1)
    end

    it 'returns 5 times counted buz' do
      expect(subject[:buz]).to eq(5)
    end

    it 'returns 3 times counted fiz' do
      expect(subject[:fiz]).to eq(3)
    end

    it 'returns 11 times counted number' do
      expect(subject[:number]).to eq(11)
    end
  end
end
