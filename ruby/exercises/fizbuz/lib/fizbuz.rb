class FizBuz
  IS_DIVERSELY = 0

  def initialize
    @result = { fiz: 0, buz: 0, fizbuz: 0, number: 0 }
    @range  = (1..20).to_a
  end

  def call
    result.tap do
      range.each do |number|
        trigger_fizbuz && next if diversely_by(number, 5) && diversely_by(number, 3)
        trigger_fiz && next if diversely_by(number, 5)
        trigger_buz && next if diversely_by(number, 3)

        trigger_number
      end
    end
  end

  private

  def diversely_by(number, target)
    number % target === IS_DIVERSELY
  end

  %i[fiz buz fizbuz number].each do |type|
    define_method "trigger_#{type}" do
      @result[type] += 1
    end
  end

  attr_reader :result, :range
end
