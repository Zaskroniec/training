# frozen_string_literal: true

require 'humanize'

class Calc
  OPERATIONS = [
    { name: 'plus',     action: '+' },
    { name: 'minus',    action: '-' },
    { name: 'divide',   action: '/' },
    { name: 'multiply', action: '*' }
  ].freeze

  def initialize
    @combination = []
    @operation   = nil
    @result      = nil
  end

  (0..100).each do |number|
    define_method number.humanize.split('-').join('_') do
      self.tap do
        combination << number
        calculate
      end
    end
  end

  OPERATIONS.each do |hash|
    define_method hash[:name] do
      self.tap { @operation = hash[:action] }
    end
  end

  private

  def calculate
    @result = combination.reduce(&:"#{@operation}")
  end

  attr_reader :combination
end

p Calc.new.ninety_two.plus.one
p Calc.new.six.minus.one
p Calc.new.six.divide.two
p Calc.new.six.multiply.six
