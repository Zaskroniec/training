# README

## Requirements

* Ruby version: `2.6.5`
* PostgreSQL version: `11.4`

## Setup

* clone repository
* go to cloned repository in shell
* run `bundle install`
* run `rake db:create` && `rake db:migrate` or `rake db:setup`
* run `rails s`
