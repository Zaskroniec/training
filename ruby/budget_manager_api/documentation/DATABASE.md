# Database

## Account

Table columns

|Column name|Type|Index|
|--|--|--|
|Hash Password|`String` (encrypted hash)|
|Name|`String`|

## User

|Column name|Type|Index|
|--|--|--|
|Email|`String` (encrypted hash)|Yes|
|First name|`String`|
|Last name|`String`|
|Account ID|`UUID`|Yes|

## Plan

|Column name|Type|Index|
|--|--|--|
|Name|`String`|
|Year|`String`|Yes|
|Account ID|`UUID`|Yes|

## Month

|Column name|Type|Index|
|--|--|--|
|Name|`String`|
|Plan ID|`UUID`|Yes|

## Item

|Column name|Type|Index|
|--|--|--|
|Name|`String`|
|Month ID|`UUID`|Yes|
|Category ID|`UUID`|Yes|
|Type|`String` (**income/outcome**)|Yes|

## Category

|Column name|Type|Index|
|--|--|--|
|Name|`String`|Yes|
|Account ID|`UUID`|Yes|
